import hudson.model.*
import jenkins.model.*
env.BRANCH_WITHOUT_TYPE = env.BRANCH_NAME.replaceAll( /release\/|hotfix\//, '' )
env.BRANCH_URL_CODED = env.BRANCH_NAME.replaceAll( /\//, '%252F' )
  
pipeline{
  agent any
  options {
    skipDefaultCheckout()
    timestamps()
  }
  parameters {
    string(name: 'inputReleaseDate', defaultValue: "${env.BRANCH_WITHOUT_TYPE}", description: 'Enter release date in YYYY.MM.DD format (REQUIRED)')
    choice(name: 'inputEnvType', choices: "<select>\nDEV\nDIT\nPROD", description: "Select the environment type (REQUIRED)")
    choice(name: 'inputDeployType', choices: "<select>\nNORMAL\nEMERGENCY\nMAINTENANCE", description: "Select the type of deploy (REQUIRED)")
    string(name: 'inputEnvnameList', defaultValue: "", description: '	inputEnvnameList')
    
 }
  stages{
    stage('Loading groovy file'){
      steps{
        script{
            dir('Deployment') { 
              checkout scm
              sh 'cat ear-customize.yaml >> project.yaml'
              props = readYaml file: 'project.yaml'
              stash includes: "project.yaml", name: 'AppStash' 
            }
            first = load './Deployment/first.groovy' 
            first.runDeployPipeline()   
        }
      }
    }
  }
 }

//**************************************************END***************************************************