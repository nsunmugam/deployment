import java.text.SimpleDateFormat
import com.cloudbees.groovy.cps.NonCPS

/*----------------------------------------------------------------------------------------------------------------------
  GIT REPO FUNCTIONS
  --------------------------------------------------------------------------------------------------------------------*/

def setupBuildRepos(props){
  sh 'git config --global http.sslverify false'

dir('Deployment') {
      props = readYaml file: 'project.yaml'
      stash includes: "project.yaml", name: 'AppStash'
    }
  }
}

/*----------------------------------------------------------------------------------------------------------------------
  EAR BUILD PIPELINE
  --------------------------------------------------------------------------------------------------------------------*/
  
def runBuildPipeline(props){
 //artifactRepoURL = scm.getUserRemoteConfigs()[0].getUrl()
  //artifactRepoName = artifactRepoURL.tokenize('/')[4].split("\\.")[0]
  env.BRANCH_WITHOUT_TYPE = env.BRANCH_NAME.replaceAll( /release\/|hotfix\//, '' )

  setupBuildRepos(props)

  buildVersion = env.BRANCH_WITHOUT_TYPE
  buildNumber = env.BUILD_NUMBER

 for (ix = 0; ix < props.environments.size();ix++) {
    if (props.environments[ix].envType.equalsIgnoreCase('QAT')){
  globalReleaseDate = props.environments[ix].releaseDate
  localBuildVersion = buildVersion
        if (localBuildVersion.toString().equalsIgnoreCase( globalReleaseDate.toString() )) {
          currentEnv = props.environments[ix]
          env.inputReleaseDate = buildVersion
          env.inputEar = artifactRepoName
          //runDevDeployStages(props, currentEnv)
        }
      }
    }
  }
}
/*----------------------------------------------------------------------------------------------------------------------
  DEPLOY PIPELINE
  --------------------------------------------------------------------------------------------------------------------*/
def runDeployPipeline(props){
  
  buildVersion = env.BRANCH_WITHOUT_TYPE
  buildNumber = env.BUILD_NUMBER

  
startQatDeployEnvsYaml()
    for (eix = 0; eix < props.environments.size();eix++) {
    def currentEnv = props.environments[eix]
    if ((currentEnv.envType.equalsIgnoreCase( env.inputEnvType )) && (currentEnv.releaseDate.equalsIgnoreCase( env.inputReleaseDate ))){
      // If environment type and releaseDate matches the given envType and releaseDate in input  parameters
      if (env.inputEnvnameList != "DEFAULT"){
        envMatches = false
        envNamesSplit = env.inputEnvnameList.tokenize(",");
        for (i = 0; i < envNamesSplit.size();i++) {
          if (currentEnv.envId.equalsIgnoreCase( envNamesSplit[i] )){
            //if a custom inputEnvnameList is given in input, check if currentEnv name is present in that list.
            envMatches = true
          }
        }
        if (envMatches == false){
          echo "Deployment is not requested in ${currentEnv.envId}"
          continue
        }
      }
      releaseDateFound = true
      echo "Found environment with matching release date: ${currentEnv}"
    }
}
def startQatDeployEnvsYaml(){
  echo "Writing QatDeployEnvs.yaml"
  sh """
    echo environments: > './Deployment/QatDeployEnvs.yaml'
  """
}

return this;